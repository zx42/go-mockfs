package mockfs

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"runtime"
	"strings"

	"github.com/pkg/errors"
)

type (
	FE struct {
		Name    string
		Content []byte
		Kids    []FE
	}
	FSys struct {
		dir     []FE
		baseDir string
	}
)

const (
	prefix = "go-mockfs-"
)

func F(name string) FE {
	return FE{Name: name}
}

func FC(name string, content string) FE {
	return FE{Name: name, Content: []byte(content)}
}

func D(name string, kids ...FE) FE {
	return FE{Name: name, Kids: kids}
}

func FS(testName string, fes ...FE) FSys {
	tmpDir, err := ioutil.TempDir("", prefix)
	if err != nil {
		panic(errors.Wrap(err, "getting tempdir"))
	}
	dir := filepath.Join(tmpDir, testName)
	return Fs(dir, fes...)
}

func Fs(dir string, fes ...FE) FSys {
	return FSys{baseDir: dir, dir: fes}
}

func (fs FSys) FS(fes ...FE) FSys {
	return Fs(fs.baseDir, fes...)
}

func (fs FSys) Path(pathElems ...string) string {
	pathL := append([]string{fs.baseDir}, pathElems...)
	return filepath.Join(pathL...)
}

func (fs FSys) Create() FSys {
	err := os.MkdirAll(fs.baseDir, 0777)
	if err != nil {
		panic(errors.Wrap(err, "Creating base dir "+fs.baseDir))
	}

	for _, fe := range fs.dir {
		err = fe.Create(fs.baseDir)
		if err != nil {
			panic(errors.Wrap(err, "Creating file tree in "+fs.baseDir))
		}
	}
	return fs
}

func (fs FSys) IsDir() bool {
	return true
}

func (fe FE) IsDir() bool {
	return len(fe.Kids) > 0
}

func (fe FE) Create(baseDir string) error {
	fePath := filepath.Join(baseDir, fe.Name)
	if fe.IsDir() {
		err := os.Mkdir(fePath, 0777)
		if err != nil && !os.IsExist(err) {
			panic(errors.Wrap(err, "Creating dir "+fePath))
		}
		for _, kid := range fe.Kids {
			err := kid.Create(fePath)
			if err != nil {
				return errors.Wrap(err, "Creating kid "+fe.Name)
			}
		}
		return nil
	}

	f, err := os.Create(fePath)
	if err != nil {
		return errors.Wrap(err, "Creating file "+fe.Name)
	}
	defer f.Close()

	_, err = f.Write(fe.Content)
	if err != nil {
		return errors.Wrap(err, "Writing file "+fe.Name)
	}
	return nil
}

func (fs FSys) Verify() error {
	for _, fe := range fs.dir {
		err := fe.Verify(fs.baseDir)
		if err != nil {
			return errors.Wrapf(err, "Verifying file tree in %q # %#v", fs.baseDir, fs)
		}
	}
	return nil
}

func (fe FE) Verify(baseDir string) error {
	fePath := filepath.Join(baseDir, fe.Name)
	if fe.IsDir() {
		for _, kid := range fe.Kids {
			err := kid.Verify(fePath)
			if err != nil {
				return errors.Wrap(err, "Verifying kid "+fe.Name)
			}
		}
		return nil
	}

	f, err := os.Open(fePath)
	if err != nil {
		return errors.Wrap(err, "opening file "+fe.Name)
	}
	defer f.Close()

	// if fe.Name == JournalFileName {
	// 	return nil
	// }

	buf := make([]byte, len(fe.Content)+42000)
	n, err := f.Read(buf)
	if err != nil {
		if err != io.EOF {
			err = errors.Wrapf(err, "Reading file %q # %#v", fe.Name, fe)
			return err
		}
	}
	if n != len(fe.Content) || bytes.Compare(buf[:n], fe.Content) != 0 {
		if n > 100 {
			n = 100
		}
		return errors.New(fmt.Sprintf("Expected '%s' but found '%s'", fe.Content, buf[:n]))
	}
	return nil
}

func (fs FSys) Discard() {
	if runtime.GOOS == "windows" {
		return
	}

	rmDir := fs.baseDir
	if !strings.HasPrefix(filepath.Base(rmDir), prefix) {
		rmDir = filepath.Dir(rmDir)
	}
	lg.Debug("CLEAN UP: os.RemoveAll()", "rmDir", rmDir)
	err := os.RemoveAll(rmDir)
	if err != nil {
		// err = errors.Wrapf(err, "Discard base dir '%s'", fs.baseDir)
		lg.Error("mockfs.FSys.Discard()", "err", err, "baseDir", fs.baseDir)
	}
}
