module gitlab.com/zx42/go-mockfs

go 1.13

// replace gitlab.com/zx42/go-deps => ../go-deps

require (
	github.com/pkg/errors v0.8.1
	gitlab.com/zx42/go-deps v0.0.3
)
