# go-mockfs

A little helper library to test go code which uses file system functionality.
It temporarily creates actual files and folders in the file system.

## Usage

see `usage_test.go`
