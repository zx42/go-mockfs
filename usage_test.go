package mockfs

import (
	"os"
	"path/filepath"
	"runtime"
	"testing"
)

func TestMockfsSample(t *testing.T) {
	// define a subfolder `d1` containing an empty file `f1`
	dir := D("d1", F("f1"))
	// create a mock file system named `test-1` with `d1` inside
	fs := FS("test-1", dir).Create()

	// ... do some file system operations
	f1 := filepath.Join(fs.Path(), "d1", "f1")
	f2 := filepath.Join(fs.Path(), "d1", "f2")
	_ = os.Rename(f1, f2)

	// define the target state
	d1f2 := D("d1", F("f2"))
	fs2 := fs.FS(d1f2)

	// verify the target state
	err := fs2.Verify()
	if err != nil {
		t.Errorf("Test in(%s) failed: %s", fs.Path(), err)
		return
	}

	// cleanup: failes on windows
	if runtime.GOOS != "windows" {
		fs.Discard()
	}
}
